<?php
header("Access-Control-Allow-Origin: * ");
header("Access-Control-Allow-Methods: GET, OPTIONS");
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Api_con extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->library('general_model');
    }

	public function sendEmailUnduh_get($id){
    	$config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'irzanafnis@gmail.com',
        'smtp_pass' => 'irzanafnis264',
        'mailtype'  => 'html',
        'charset'   => 'iso-8859-1'
        );
        
        $data = $this->general_model->getWhere('tbl_buku',array('id_buku' => $id))->row();
        // $data = 123;
        $html = $this->load->view('email_unduh',$data,TRUE);
        $this->load->library('email', $config);
        
        $this->email->set_newline("\r\n");

            $this->email->from('mygmail@gmail.com', 'Perpustakaan Uhamka');
            // $this->email->to($this->session->email);
            $this->email->to($this->session->email);

            $this->email->subject('Link Unduh');
            $this->email->message($html);
        // Set to, from, message, etc.
                
        	if($this->email->send()){
        		echo "<script>window.close()</script>";
        	}     
    } 


    public function logout_get(){
        $exec = $this->session->sess_destroy();
        $this->response(array('response'=>'ok'));
    }

     function login_post(){
        $email    = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array(
                'no_id'=>$email,
                'password' =>md5($password)
            );

        $query=$this->general_model->getWhere("tbl_user",$where);
        if($query->num_rows()>0){
            $sess = $query->row();
            $data = array(
                'id_user' => $sess->id_user,
                'nama_lengkap' => $sess->nama_lengkap,
                'role'=>$sess->role,
                'email'=> $sess->email
            );
            $this->session->set_userdata($data);
            $this->response(array('response'=>'ok','data'=>$data));
        }else{
            $this->response(array('response'=>'failed'));
        }
    }

    function auth_get(){
        if($this->session->id_user!=NULL){
            $this->response(array('response'=> 'ok'));
        }else{
            $this->response(array('response'=>'failed'));
        }
        // echo $this->session->id_user;
    }
    
	 
}