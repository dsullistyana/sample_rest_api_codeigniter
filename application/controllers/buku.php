<?php
header("Access-Control-Allow-Origin: * ");
header("Access-Control-Allow-Methods: GET, OPTIONS");
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Buku extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->library('general_model');
    }

	/**
	Untuk mengeluarkan data user.
	**/
	public function index_get($id=''){
		if($id!=''){
			$data=$this->general_model->getWhere('tbl_buku',array('id_buku'=>$id))->result();
		}else{
			$data=$this->general_model->getAll('tbl_buku')->result();
		}
		//response request
		if($data){
			$this->response(array('data' => $data,'response'=>'ok'),200);
		}else{
			$this->response(array('response'=>'failed'),200);
		}
	}

	public function index_put($id=''){
		if($id!=''){
			$cek 	= $data=$this->general_model->getWhere('tbl_buku',array('id_buku'=>$id));
			if($cek->num_rows() > 0){
				$data_update	= array(
					'judul'		   => $this->put('judul'),
					'penulis' 	   => $this->put('penulis'),
					'penerbit' 	   => $this->put('penerbit'),
					'tanggal' 	   => $this->put('tanggal'),
					'harga' 	   => $this->put('harga'),
					'gambar' 	   => $this->put('gambar'),
					'sinopsis'	   => $this->put('sinopsis'),
					'id_kategory'  => $this->put('id_kategory'), 
				);
				$data 			= $this->general_model->UpdateWhereData('tbl_buku',$data_update,array('id_buku' => $id));
			}
			if($data){
				$this->response(array('data' => $data,'response'=>'ok'),200);
			}else{
				$this->response(array('response'=>'failed'),200);
			}
		}else{
			$this->response(array('response'=>'failed,Id User Not Available'),200);
		}	
	}

	public function index_delete(){
		$id=$this->delete('id_buku');
		$data=$this->general_model->deleteWhereData('tbl_buku',array('id_buku' => $id));
		if($data){
			$this->response(array('response'=>'ok'),200);
		}else{
			$this->response(array('response'=>'failed'),200);
		}
	}

	public function index_post(){
		$data_insert	= array(
					'judul'		   => $this->post('judul'),
					'penulis' 	   => $this->post('penulis'),
					'penerbit' 	   => $this->post('penerbit'),
					'tanggal' 	   => $this->post('tanggal'),
					'harga' 	   => $this->post('harga'),
					'gambar' 	   => $this->post('gambar'),
					'sinopsis'	   => $this->post('sinopsis'),
					'id_kategory'  => $this->post('id_kategory'), 
				);

		$data=$this->general_model->insertData('tbl_buku',$data_insert);
		
		if($data){
			$this->response(array('response'=>'ok'),200);
		}else{
			$this->response(array('response'=>'failed'),200);
		}
	}

}