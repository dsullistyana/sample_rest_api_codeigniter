<?php
		// session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth1 extends Ci_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->library('session');

    }

    public function index_get(){
        $dataArray=array();
        if(isset($this->session->username)){
            $dataArray['response']='ok';
        }else{
            $dataArray['response']='failed';
        }

    }

    public function getSessionUser(){
    	$username=$this->input->post('username');
    	$password=md5($this->input->post('password'));

    	$this->db->where('username',$username);
    	$this->db->where('password',$password);
    	$data=$this->db->get('tbl_user');
    	$dataParse=$data->result_array();
        $dataArray=array();
    	if($data->num_rows() > 0){
            $this->session->set_userdata('username',$username);
            $dataArray['response']='ok';
    	}else{
            $dataArray['response']='failed';
    	}
        $this->output->set_output(json_encode($dataArray),JSON_PRETTY_PRINT);
    }

    public function logout(){
        // session_start();
        session_destroy();
    }

}